import numpy as np
from scipy import signal as sig
import matplotlib.pyplot as plt
from scipy import fft
import control as ctrl
import librosa
from D_signal import SignalProcessing as dsp
from fpdf import FPDF

a_file = open('global_specs.txt')

"""
Explanation of specification file variables:


--- FILE READING ---

    signal_source : Use input signal or generated signal? Possible: wav, generate

    file_name : WAV-filename

    target_Samp_Freq : Target sampling frequency (Hz) of the .wav file


 --- SIGNAL GENERATION ---
    
    signal_type : Type of signal if generation is desired. Possible: rect, tone, tone-complex, noise
    
    samp_freq : Sampling frequency (Hz) of generated signal

    signal_duration : Overall duration of the signal (in s)
    
    periodicity : Periodicity of the signal (s) - only applicable for "rect". The width of each rectangle should be half the period. 
    
    tone_freqs : List of frequency/-ies of the tone/tone complex (only first if signal_type == 'tone')


--- FREQUENCY ANALYSIS ---

    spectral_resolution : Spectral resolution (Hz)
    
    window_type : Window type for time frame DFT/STFT. Possible: hann, hamming, rect
    
    stft_overlap : Overlap for STFT (%)

    
--- FILTERING ---

    filtering_type : Use FIR or IIR?
    
    filter_order : filter order
    
    approx_method : Approximation method. Possible: butterworth, chebychevI, chebychevII, cauer
    
    approx_method_type : Type of approximation method. Possible: lp, hp, bp, bs
    
    cut_off_freqs : List of cut-off frequency/-ies (Hz)


--- PLOTTING ---

    - Limits of axis in time and spectrum. -

        x_lim_time : xlim - time (s)
    
        y_lim_amp : ylim - amplitude (a.u.)
    
    - frequency on linear or log axis?

        x_axis_scale : log in x? Possible: lin, log

        y_axis_scale : log in y? Possible: lin, log
    
        x_lim_freq : xlim - frequency (Hz) - we go only positive frequencies. If nyquist, then fs/2
    
        y_lim_val : ylim - (a.u.) 
    

"""

var_dict = {
    'signal_source' : 0 , 'file_name' : 0 , 'target_samp_freq' : 0 , 'signal_type' : 0 , 
    'samp_freq' : 0 , 'signal_duration' : 0 , 'periodicity' : 0 , 'tone_freqs' : 0 , 
    'spectral_resolution' : 0 , 'window_type' : 0 , 'stft_overlap' : 0 , 'filtering_type' : 0 , 
    'filter_order' : 0 , 'approx_method' : 0 , 'approx_method_type' : 0 , 'cut_off_freqs' : 0 , 
    'x_lim_time' : 0 , 'y_lim_amp' : 0 , 'x_axis_scale' : 0 , 'y_axis_scale' : 0 , 'x_lim_freq' : 0 , 
    'y_lim_val' : 0
    }

dict_entries = ['signal_source' , 'file_name' , 'target_samp_freq' , 'signal_type' , 
    'samp_freq' , 'signal_duration' , 'periodicity' , 'tone_freqs' , 
    'spectral_resolution' , 'window_type' , 'stft_overlap' , 'filtering_type' , 
    'filter_order' , 'approx_method' , 'approx_method_type' , 'cut_off_freqs' , 'x_lim_time' , 
    'y_lim_amp' , 'x_axis_scale' , 'y_axis_scale' , 'x_lim_freq' , 
    'y_lim_val']


def is_num(num):
    try:
        float(num)
        return True
    
    except:
        return False
  

i = 0
string = '%'
for line in a_file:
    if string not in line:
        if '\n' in line:
            line = line.replace('\n' , '')

        if line != '':
            line = line.replace(' ' , '')

            if is_num(line):
                line = float(line)

            var_dict[dict_entries[i]] = line
            
            i += 1


var_dict['tone_freqs'] = var_dict['tone_freqs'].split(',')
var_dict['cut_off_freqs'] = var_dict['cut_off_freqs'].split(',')


for val in range(len(var_dict['tone_freqs'])):
    var_dict['tone_freqs'][val] = float(var_dict['tone_freqs'][val])

for val in range(len(var_dict['cut_off_freqs'])):
    var_dict['cut_off_freqs'][val] = float(var_dict['cut_off_freqs'][val])



#print(var_dict)

a_file.close()


# Further document processing

var_dict['filter_order'] = int(var_dict['filter_order'])

if var_dict['x_lim_freq'] == 'nyquist':
    var_dict['x_lim_freq'] = var_dict['samp_freq'] / 2



# -------------- PREPARING PDF ------------------ #

pdf = FPDF()
pdf.add_page()
pdf.set_font("Arial","B",28)
pdf.cell(ln=1, h = 14.0, align='L', w=0, txt="DSP Assignment outputs", border=0)

# PDF FONT STUFF
def pdf_subtitle(text):
    pdf.set_font("Arial","B", size=20)
    pdf.cell(h = 8.0, align='L', w=0, txt=text, border=0)

def pdf_subsubtitle(text):
    pdf.set_font("Arial","B", size=16)
    pdf.cell(h = 8.0, align='L', w=0, txt=text, border=0)

def pdf_regtxt(text):
    pdf.set_font("Arial", size=12)
    pdf.write(h = 8.0,txt=text)


# # comments can be added this way;
# pdf.write(4,"Hello Emil ")

# #To add images/plots this is how we can do this; 
# # Create a simple plot using matplotlib
# x_values = [1, 2, 3, 4, 5]
# y_values = [10, 20, 15, 25, 30]

# plt.plot(x_values, y_values)
# plt.xlabel('X-axis')
# plt.ylabel('Y-axis')
# plt.title('Sample Plot')

# # Save the plot to a temporary file
# with tempfile.NamedTemporaryFile(delete=False, suffix=".png") as temp_file:
#     plt.savefig(temp_file.name, format='png')

# # Move the cursor to the desired position in the PDF and add the image
# pdf.image(temp_file.name, x=30, y=30, w=100)

# # Close the temporary file
# temp_file.close()

# #To print the output
# pdf.output("file.pdf")





# ------------------------ Signal Generation / Wav-reading ---------------------------------#

if var_dict['signal_source'] == 'wav':

    pdf_subtitle("Wav file inspection")

    pdf_regtxt("Below is the time/frequency plot of the read .wav file. The signal has been re-sampled at {}.".format(var_dict['target_samp_freq']))

    """
    Enter the filepath of the .wav file into the function, as well as a new sampling rate if desired.

    Eg.     librosa.load('/filepath.wav' , sr=new_sampling_rate)

    'signal' is an array of values , 'sampling rate' is an integer

    """

    signal_arr, var_dict['samp_freq'] = librosa.load(var_dict['file_name'] , sr = var_dict['target_samp_freq'])

    time_arr = np.arange(0, len(signal_arr)/var_dict['samp_freq'], 1/(var_dict['samp_freq']))




elif var_dict['signal_source'] == 'generate':

    pdf_subtitle("Signal Generation")
    
    """
    Match case for the type of signal to be generated.

    Signal types : Rect , Tone , Tone-complex , Noise

    """

    match(var_dict['signal_type']):
        case 'rect':

            var_dict['tone_freqs'] = var_dict['tone_freqs'][0]

            signal_arr, time_arr = dsp.make_rect(var_dict['tone_freqs'], 
                                                    var_dict['signal_duration'] , 
                                                    var_dict['samp_freq'] ,
                                                    var_dict['periodicity'] ,
                                                    amplitude=1)
            
            pdf_regtxt("""This signal is a rect signal with frequency {}, periodicity {} and duration {}, sampled with a sampling frequency of {}""".format(var_dict['tone_freqs'] , var_dict['periodicity'], var_dict['signal_duration'] , var_dict['samp_freq']))
            
        case 'tone':

            var_dict['tone_freqs'] = var_dict['tone_freqs'][0]

            signal_arr , time_arr = dsp.make_signal(var_dict['tone_freqs'] ,
                                                    var_dict['signal_duration'] ,
                                                    1 ,
                                                    var_dict['samp_freq'] ,
                                                    )
            
            pdf_regtxt("""This signal is a tone signal with frequency {} and duration {}, sampled with a sampling frequency of {}"""
                  .format(var_dict['tone_freqs'] , 
                        var_dict['signal_duration'] , 
                        var_dict['samp_freq']
                        ))
            
        case 'tone-complex':
            signal_arr , time_arr = dsp.tone_complex(var_dict['tone_freqs'] , 
                                                     var_dict['signal_duration'] , 
                                                     var_dict['samp_freq'] ,
                                                     amplitude=1)
            
            pdf_regtxt(("""This signal is a tone-complex with frequencies {} and duration {}, sampled with a sampling frequency of {}."""
                  .format(var_dict['tone_freqs'] , 
                        var_dict['signal_duration'] , 
                        var_dict['samp_freq'])
                      ))

        case 'noise':
            signal_arr , time_arr = dsp.make_noise(var_dict['signal_duration'] , 
                                                    var_dict['samp_freq'] ,
                                                    amplitude=1)
            
            pdf_regtxt("""This signal is random noise, with a duration {}, \nsampled with a sampling frequency of {}"""
                  .format(var_dict['signal_duration'] , 
                        var_dict['samp_freq']))



# --------------- Signal Plotting ----------------- #

match(var_dict['signal_source']):
    case "wav":
        dsp.plot_signal(signal=signal_arr, 
                        time=time_arr,
                        x_lim=var_dict['x_lim_time'],
                        y_lim=var_dict['y_lim_amp'],
                        type='plot', 
                        title='Signal before filtering',
                        save_file="orig_signal.png")

        pdf.image("orig_signal.png", x=30, y=pdf.y+10, w=130)

    case "generate":
        dsp.plot_signal(signal=signal_arr, 
                        time=time_arr, 
                        type='plot',
                        x_lim=var_dict['x_lim_time'],
                        y_lim=var_dict['y_lim_amp'],
                        title='Signal before filtering',
                        save_file="orig_signal.png")

        pdf.image("orig_signal.png", x=30, y=pdf.y+10, w=130)


pdf.set_xy(10, pdf.y+110) ### MOVE TEXT



# --------------- STFT and DFT BEFORE filtering -------------- #

pdf_regtxt("Below are the power spectra for the signal, first using the Discrete Fourier Transform, and then using the Short-Time Fourier Tranform.")

match(var_dict['signal_source']):

    case "wav":
        dsp.pspectrum(x=signal_arr, fs=var_dict['samp_freq'], xlim=var_dict['x_lim_freq'], ylim=var_dict['y_lim_val'], name="DFT_p_spec_wav.png")

        pdf.image("DFT_p_spec_wav.png", x=30, y=pdf.y+10, w=130)

        dsp.stft_pspectrum(x=signal_arr, fs=var_dict['samp_freq'], xlim=var_dict['x_lim_freq'], ylim=var_dict['y_lim_val'], overlap=var_dict['stft_overlap'], name="STFT_p_spec_wav.png")

        pdf.add_page()

        pdf.image("STFT_p_spec_wav.png", x=30, y=pdf.y+10, w=130)

    case "generate":
        dsp.pspectrum(x=signal_arr, fs=var_dict['samp_freq'], xlim=var_dict['x_lim_freq'], ylim=var_dict['y_lim_val'], name="DFT_p_spec_gen.png")

        pdf.image("DFT_p_spec_gen.png", x=30, y=pdf.y+10, w=130)

        dsp.stft_pspectrum(x=signal_arr, fs=var_dict['samp_freq'], xlim=var_dict['x_lim_freq'], ylim=var_dict['y_lim_val'], overlap=var_dict['stft_overlap'], name="STFT_p_spec_gen.png")

        pdf.add_page() 

        pdf.image("STFT_p_spec_gen.png", x=30, y=pdf.y+10, w=130)




pdf.set_xy(10, pdf.y+110) ### MOVE TEXT


# -------------------- FILTERING ----------------------- #

pdf_subtitle("Filtering")

match(var_dict['filtering_type']):

    case 'iir':
        signal_filtered_IIR, IIR_filter = dsp.filter_IIR(signal=signal_arr , 
                                        order=var_dict['filter_order'] , 
                                        cutoff=var_dict['cut_off_freqs'] ,
                                        sampling_freq=var_dict['samp_freq'] ,
                                        approx_method=var_dict['approx_method'],
                                        filter_type=var_dict['approx_method_type'])
        
        pdf_regtxt("We now build an {}-filter. The order is {}, the filter type is {}, and the approximation method is {}. The cutoff frequency/-ies used is/are {}. The frequency response is shown below.".format(
            var_dict['filtering_type'],
            var_dict['filter_order'],
            var_dict['approx_method'],
            var_dict['approx_method_type'],
            var_dict['cut_off_freqs']
        ))


    case 'fir':
        signal_filtered_FIR, FIR_filter = dsp.filter_FIR(signal=signal_arr,
                                                       cutoff=var_dict['cut_off_freqs'],
                                                       n=var_dict['filter_order'],
                                                       fs=var_dict['samp_freq'],
                                                       filter_type=var_dict['approx_method_type'],
                                                       window=var_dict['window_type'])

        pdf_regtxt("We now build an {}-filter. The order is {}, the filter type is {}, and the approximation method is {}. The cutoff frequency/-ies used is/are {}. The frequency response is shown below.".format(
            var_dict['filtering_type'],
            var_dict['filter_order'],
            var_dict['approx_method'],
            var_dict['approx_method_type'],
            var_dict['cut_off_freqs']
        ))







# --------------- Filter Characteristics Plotting ----------------- #

match(var_dict['filtering_type']):
    case 'iir':
        w, h = sig.freqz(b = IIR_filter[0], a=IIR_filter[1], fs=var_dict['samp_freq'])
        
        fig, ax1 = plt.subplots()
        ax1.set_title('FIR-filter frequency response')
        ax1.plot(w, 20 * np.log10(abs(h)), 'b')
        ax1.set_ylabel('Amplitude [dB]', color='b')
        ax1.set_xlabel('Frequency [Hz]')
        ax2 = ax1.twinx()
        angles = np.unwrap(np.angle(h))
        ax2.plot(w, angles*180/np.pi, 'g')
        ax2.set_ylabel('Angle (radians)', color='g')
        ax2.grid(True)
        ax2.axis('tight')
        plt.savefig('fir_freq_response.png', bbox_inches='tight')
        pdf.image('fir_freq_response.png', x=30, y=pdf.y+10, w=140)
        plt.show()
        
        

    case 'fir':
        w, h = sig.freqz(b = FIR_filter, a=1, fs=var_dict['samp_freq'])
        
        fig, ax1 = plt.subplots()
        ax1.set_title('FIR-filter frequency response')
        ax1.plot(w, 20 * np.log10(abs(h)), 'b')
        ax1.set_ylabel('Amplitude [dB]', color='b')
        ax1.set_xlabel('Frequency [Hz]')
        ax2 = ax1.twinx()
        angles = np.unwrap(np.angle(h))
        ax2.plot(w, angles*180/np.pi, 'g')
        ax2.set_ylabel('Angle (radians)', color='g')
        ax2.grid(True)
        ax2.axis('tight')
        plt.savefig('fir_freq_response.png', bbox_inches='tight')
        pdf.image('fir_freq_response.png', x=30, y=pdf.y+10, w=140)
        plt.show()

#pdf.set_xy(10, pdf.y+110) ### MOVE TEXT      

pdf.add_page()
# --------------- Filtered Signal Plotting ----------------- #



pdf_regtxt('The filtered signal is shown below, together with the original signal.')

match(var_dict['signal_source']):
    case "wav":
        if var_dict['filtering_type'] == "iir":
            dsp.plot_signal(signal=signal_filtered_IIR, signal2=signal_arr, time2=time_arr,
                    time=time_arr, type='plot', x_lim=var_dict['x_lim_time'], y_lim=var_dict['y_lim_amp'],
                    title='Signal after {}-filtering, using an order {} {} filter'.format(
                        var_dict['filtering_type'],
                        var_dict['filter_order'],
                        var_dict['approx_method']),
                        save_file="filt_signal_iir_wav.png")
            
            pdf.image("filt_signal_iir_wav.png", x=30, y=pdf.y+10, w=140)
            
        elif var_dict['filtering_type'] == "fir":
            dsp.plot_signal(signal=signal_filtered_FIR, signal2=signal_arr, time2=time_arr,
                    time=time_arr, type='plot', x_lim=var_dict['x_lim_time'], y_lim=var_dict['y_lim_amp'],
                    title='Signal after {}-filtering, using an order {} filter'.format(
                        var_dict['filtering_type'],
                        var_dict['filter_order']),
                        save_file="filt_signal_fir_wav.png")
            
            pdf.image("filt_signal_fir_wav.png", x=30, y=pdf.y+10, w=140)

    case "generate":
        if var_dict['filtering_type'] == "iir":
            dsp.plot_signal(signal=signal_filtered_IIR, signal2=signal_arr, time2=time_arr,
                    time=time_arr, type='plot', x_lim=var_dict['x_lim_time'], y_lim=var_dict['y_lim_amp'],
                    title='Signal after {}-filtering, using an order {} {} filter'.format(
                        var_dict['filtering_type'],
                        var_dict['filter_order'],
                        var_dict['approx_method']),
                        save_file="filt_signal_iir_gen.png")
            
            pdf.image("filt_signal_iir_gen.png", x=30, y=pdf.y+10, w=140)
            
        elif var_dict['filtering_type'] == "fir":
            dsp.plot_signal(signal=signal_filtered_FIR, signal2=signal_arr, time2=time_arr,
                    time=time_arr, type='plot', x_lim=var_dict['x_lim_time'], y_lim=var_dict['y_lim_amp'],
                    title='Signal after {}-filtering, using an order {} filter'.format(
                        var_dict['filtering_type'],
                        var_dict['filter_order']),
                        save_file="filt_signal_fir_gen.png")
            
            pdf.image("filt_signal_fir_gen.png", x=30, y=pdf.y+10, w=140)


pdf.output("DSP-Assignment-Results.pdf")