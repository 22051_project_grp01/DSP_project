import numpy as np
from scipy import signal as sig
import matplotlib.pyplot as plt
import math as m
from scipy import fft
import control as ctrl
import librosa as lib

class SignalProcessing:

    # takes signal 'x' and the frequency 'freq' and plots the frequency spectrum.
    def pspectrum(x, fs, xlim, ylim, name):
        """
        Plot the spectrogram of a signal using the short-time Fourier transform.

        Parameters:
        - x (array-like): The input signal.
        - fs (float): The sampling frequency of the signal.
        - xlim (float): The limit for the x-axis (time).
        - ylim (float): The limit for the y-axis (frequency).
        - name (str): The filename to save the plot as a PNG file.
        - freqs (array-like): The frequency range to consider in the plot.

        Returns:
        None
        """
        f, t, Sxx = sig.spectrogram(x, fs=fs)
        plt.pcolormesh(t, f, Sxx)
        plt.title('DFT Power Spectrum')
        plt.ylabel('Frequency [Hz]')
        plt.xlabel('Time [s]')
        plt.colorbar(label="Power [dB]", orientation="vertical")
        plt.xlim(0, ylim)# Frequency is now on the y-axis, hence the swap
        plt.ylim(0, xlim)
        plt.savefig(name, bbox_inches='tight')
        plt.show()

    # takes base signal, before transforms, and var_dict['samp_freq']
    def stft_pspectrum(x, fs, xlim, ylim, name, overlap):
        """
        Plot the spectrogram of a signal using the short-time Fourier transform.

        Parameters:
        - x (array-like): The input signal.
        - fs (float): The sampling frequency of the signal.
        - xlim (float): The limit for the x-axis (time).
        - ylim (float): The limit for the y-axis (frequency).
        - name (str): The filename to save the plot as a PNG file.
        - overlap (int): The number of overlapping samples between adjacent segments.

        Returns:
        None
        """
        f, t, Zxx = sig.stft(x, fs, scaling="spectrum", noverlap=overlap)
        plt.pcolormesh(t, f, np.abs(Zxx), vmin=0, vmax=np.max(x), shading='auto')
        plt.title('STFT Power Spectrum')
        plt.ylabel('Frequency [Hz]')
        plt.xlabel('Time [sec]')
        plt.colorbar(label="Power [dB]", orientation="vertical")
        plt.xlim(0, ylim) # Frequency is now on the y-axis, hence the swap
        plt.ylim(0, xlim)
        plt.savefig(name, bbox_inches='tight')
        plt.show()

        
    def get_ticks(signal, time):
        """
        Calculate the ticks per second based on a signal and corresponding time array.
    
        Parameters:
        - signal (numpy array): The signal values.
        - time (numpy array): The corresponding time values.
    
        Returns:
        - float: The ticks per second, indicating the frequency of signal changes.
        """
        if np.size(time) > 0 and max(signal) > 0:
            ticks = 1/((np.size(signal)/max(time)))
        else:
            ticks = 0
        return ticks
    
    def tone_complex(freqs, duration, sampling_freq, amplitude=1):
        """
        Generate a tone complex signal composed of multiple sinusoids.
    
        Parameters:
        - freqs (list): List of frequencies of the sinusoids.
        - duration (float): Duration of the signal in seconds.
        - sampling_freq (int): Sampling frequency in Hertz.
        - amplitude (float, optional): Amplitude of the sinusoids. Default is 1.
    
        Returns:
        - tuple: A tuple containing the generated signal and corresponding time array.
        """
        t = np.arange(0, duration, 1/(sampling_freq))

        signal = 0

        for i in range(0, len(freqs)):
            signal = signal + amplitude * np.sin(2 * np.pi * freqs[i]*t)

        return signal , t
    
    
    


    def filter_IIR(signal , cutoff , sampling_freq, approx_method, filter_type, order):

        """
        Apply an Infinite Impulse Response (IIR) filter to the input signal.

        Parameters:
        - signal (numpy array): The input signal to be filtered.
        - cutoff (float or list of floats): The cutoff frequency or frequencies of the filter.
        - sampling_freq (int): The sampling frequency of the signal.
        - approx_method (str): The approximation method for the filter design.
        - filter_type (str): The type of filter ('lp' for lowpass, 'hp' for highpass, 'bp' for bandpass, 'bs' for bandstop).
        - order (int): The order of the filter.

        Returns:
        - tuple: A tuple containing the filtered signal and the filter coefficients (numerator and denominator).

        Note:
        - For 'lp' and 'hp' filter types, cutoff can be a single value.
        - For 'bp' and 'bs' filter types, cutoff should be a list containing two values [low_cutoff, high_cutoff].
        """

        ripple = 1 #dB
        attenuation = 60 #dB
        
        cutoff = [i/(sampling_freq/2) for i in cutoff]

        match filter_type:
            case 'lp':
                filter_type = 'lowpass'
                cutoff=[cutoff[0]]
            case 'hp':
                filter_type = 'highpass'
                cutoff=[cutoff[0]]
            case 'bp':
                filter_type = 'bandpass'
            case 'bs':
                filter_type = 'bandstop'

        filt_num , filt_den = sig.iirfilter(N=order , Wn=cutoff, rp=ripple, rs=attenuation, btype=filter_type , ftype=approx_method)

        filtered_signal = sig.lfilter(filt_num, filt_den, signal)

        return filtered_signal, [filt_num, filt_den]
    


    def filter_FIR(signal, cutoff , n, fs, filter_type, window):
        """
        Apply a Finite Impulse Response (FIR) filter to the input signal.

        Parameters:
        - signal (numpy array): The input signal to be filtered.
        - cutoff (float or list of floats): The cutoff frequency or frequencies of the filter.
        - n (int): The number of taps (filter order).
        - fs (float): The sampling frequency of the signal.
        - filter_type (str): The type of filter ('lp' for lowpass, 'hp' for highpass, 'bp' for bandpass, 'bs' for bandstop).
        - window (str): The window function to be used in filter design.

        Returns:
        - tuple: A tuple containing the filtered signal and the filter coefficients.

        Note:
        - For 'lp' and 'hp' filter types, cutoff can be a single value.
        - For 'bp' and 'bs' filter types, cutoff should be a list containing two values [low_cutoff, high_cutoff].
        """
        match filter_type:
            case 'lp':
                filter_type = 'lowpass'
                cutoff=[cutoff[0]]
            case 'hp':
                filter_type = 'highpass'
                cutoff=[cutoff[0]]
            case 'bp':
                filter_type = 'bandpass'
            case 'bs':
                filter_type = 'bandstop'

        filt = sig.firwin(numtaps=n, 
                          cutoff=cutoff, 
                          window=window,
                          pass_zero=filter_type,
                          fs=fs)
        
        filtered_signal = sig.lfilter(filt, 1, signal)
                        

        return filtered_signal, filt



    
    def make_noise(duration, sampling_freq, amplitude):
        """
        Generate a random noise signal.

        Parameters:
        - duration (float): The duration of the signal in seconds.
        - sampling_freq (int): The sampling frequency of the signal.
        - amplitude (float): The amplitude of the noise signal.

        Returns:
        - tuple: A tuple containing the generated noise signal and the corresponding time values.
        """
        t = np.arange(0, duration, 1/(sampling_freq))

        signal = amplitude * np.random.randn(len(t))

        return signal , t
    
    def make_rect(freqs, duration, sampling_freq, periodicity, amplitude=1):
        """
        Generate a rectangular pulse signal.

        Parameters:
        - freqs (float): The frequency of the rectangular pulse.
        - duration (float): The duration of the signal in seconds.
        - sampling_freq (int): The sampling frequency of the signal.
        - periodicity (float): The periodicity of the rectangular pulse.
        - amplitude (float): The amplitude of the rectangular pulse.

        Returns:
        - tuple: A tuple containing the generated rectangular pulse signal and the corresponding time values.
        """
        t = np.arange(0, duration, 1/(sampling_freq))
        signal = np.where(np.mod(t * freqs, 1) < periodicity, 1, 0)

        return signal , t


    def make_signal(frequency, duration, amplitude, sampling_rate="None", phase=0, type="sin", noise=False, noise_strength=0.01):
        """
        Generate a sinusoidal or cosinusoidal signal.

        Parameters:
        - frequency (float): The frequency of the signal.
        - duration (float): The duration of the signal in seconds.
        - amplitude (float): The amplitude of the signal.
        - sampling_rate (float, optional): The sampling rate of the signal. If not specified, it is set to 2.5 times the frequency or 44100, whichever is greater.
        - phase (float, optional): The phase of the signal in radians (default: 0).
        - type (str, optional): The type of signal. Options: "sin", "cos" (default: "sin").
        - noise (bool, optional): Add random noise to the signal (default: False).
        - noise_strength (float, optional): Strength of the added noise (default: 0.01).

        Returns:
        - tuple: A tuple containing the generated signal and the corresponding time values.
        """
        if sampling_rate == "None":
            sampling_rate = frequency * 2.5
        time = np.arange(0, duration, 1/(sampling_rate))
        match type.lower():
            case "sin":
                signal = amplitude * np.sin(2 * np.pi * frequency * time + phase)
            case "cos":
                signal = amplitude * np.cos(2 * np.pi * frequency * time + phase)
            case _:
                print("Error: Invalid version")
        if noise:
            signal += np.random.normal(0, noise_strength, np.size(signal))

        return signal, time

    def make_spectrum(signal, fs=44100, part="both"):
        """
        Compute the spectrum of a signal.

        Parameters:
        - signal (numpy.ndarray): The input signal.
        - fs (float, optional): The sampling rate of the signal (default: 44100).
        - part (str, optional): The part of the spectrum to return. Options: "both", "positive", "negative", "pos", "neg" (default: "both").

        Returns:
        - tuple: A tuple containing the spectrum and the corresponding frequency values.
        """
        Y = fft.fft(signal)
        Y = Y/(len(Y))
        delta_f = fs/len(Y)
        freq = np.arange(-fs/2, fs/2, delta_f)
        y_pos = Y[0:int(len(Y)/2)];
        y_neg = Y[int(len(Y)/2):len(Y)];
        freq_neg = freq[0:int(len(freq)/2)];
        freq_pos = freq[int(len(freq)/2):len(freq)];
        Y = np.concatenate((y_neg,y_pos));
        match part.lower():
            case "both":
                return Y, freq
            case "positive":
                return y_pos, freq_pos
            case "pos":
                return y_pos, freq_pos
            case "negative":
                return y_neg, freq_neg
            case "neg":
                return y_neg, freq_neg
            case _:
                print("Error: Invalid version")

    def load_signal(file_name):
        """
        Load a signal from a WAV file.

        Parameters:
        - file_name (str): The path to the WAV file.

        Returns:
        - tuple: A tuple containing the loaded signal, the corresponding time values, and the sampling rate.
        """
        signal, fs = lib.load(file_name)
        time = np.arange(0, np.size(signal) / fs, 1 / fs)
        return signal, time, fs

    def plot_signal(signal, time, x_lim, y_lim, signal2=None, time2=None, type="stem",title="Waveform",color=["red","lightblue"],save_file=None):
        """
        Plot one or two discrete-time signals with optional legends using Matplotlib.

        Parameters:
        - signal (list or numpy.ndarray): The values of the first signal to be plotted.
        - time (list or numpy.ndarray): The corresponding time values for the first signal.
        - signal2 (list or numpy.ndarray, optional): The values of the second signal to be plotted (default: None).
        - time2 (list or numpy.ndarray, optional): The corresponding time values for the second signal (default: None).
        - type (str, optional): The type of plot to create. Options: "stem", "plot", "both" (default: "stem").
        - color (list of str, optional): Colors to use for the signals. The first color corresponds to Signal 1,
          and the second color corresponds to Signal 2 (default: ["red", "lightblue"]).
        - interval (list of float, optional): The time interval to plot (e.g., [0, 1]). If None, the entire signal is plotted (default: None).
        - save_file (str, optional): The filename to save the plot as a PNG file (e.g., "my_plot.png"). If None, the plot is not saved (default: None).

        Returns:
        None

        Example:
        plot_signal(signal, time, signal2=signal2, time2=time2, type="plot", title="Comparison of Signals", color=["blue", "green"], interval=[0, 5], save_file="plot_comparison.png")
        """
        match type.lower():
            case "stem":
                if signal2 is not None and time2 is not None:
                    plt.stem(time2, signal2, color[1], basefmt="", use_line_collection=True)
                    plt.stem(time, signal, color[0], basefmt="", use_line_collection=True)
                    #place legend above plot
                    plt.legend(["Signal 2", "Signal 1"], loc='upper right', bbox_to_anchor=(1.25, 0.15), ncol=1)
                else:
                    plt.stem(time, signal, basefmt="", use_line_collection=True)
            case "plot":
                if signal2 is not None and time2 is not None:
                    plt.plot(time2, signal2, color[1])
                    plt.plot(time, signal, color[0])
                    plt.legend(["Signal 2", "Signal 1"], loc='upper right', bbox_to_anchor=(1.25, 0.15), ncol=1)
                else:
                    plt.plot(time, signal)
            case "both":
                if signal2 is not None and time2 is not None:
                    plt.plot(time2, signal2, color[1])
                    plt.stem(time, signal, color[0], basefmt="", use_line_collection=True)
                    plt.legend(["Signal 2", "Signal 1"], loc='upper right', bbox_to_anchor=(1.25, 0.15), ncol=1)
                else:
                    plt.plot(time, signal)
                    plt.stem(time, signal, basefmt="", use_line_collection=True)
            case _:
                print("Error: Invalid version")
        plt.grid(True)
        plt.xlabel("Time (s)")
        plt.ylabel("Amplitude")
        plt.title(title)
        plt.xlim(0, x_lim)
        plt.ylim((-y_lim, y_lim))
        
        if save_file:
            plt.savefig(save_file, bbox_inches='tight')  # Use bbox_inches='tight' to ensure the legend is not cut off
        plt.show()




    def plot_spectrum(signal, freq, fs=44100, type="stem", part="Mag_Phase", interval=None):
        """
        Plot the spectrum of a signal using various representations.

        Parameters:
        - signal (numpy.ndarray): The signal for which to plot the spectrum.
        - freq (numpy.ndarray): The frequency values corresponding to the signal spectrum.
        - fs (float, optional): The sampling frequency (default: 44100).
        - type (str, optional): The type of plot to create. Options: "stem", "plot", "both" (default: "stem").
        - part (str, optional): The part of the spectrum to plot. Options: "Mag_Phase", "Real_Imag", "Bode" (default: "Mag_Phase").
        - interval (list of float, optional): The frequency interval to plot (e.g., [0, 5000]). If None, the entire spectrum is plotted (default: None).

        Returns:
        None

        Example:
        plot_spectrum(signal, freq, fs=44100, type="plot", part="Real_Imag", interval=[0, 5000])
        """
        # Compute the magnitude and phase of the FFT
        magnitude = np.abs(signal)
        phase = np.angle(signal)
        
        match part.lower():
            case "mag_phase":
                support.plot_mag_phase(magnitude, phase, freq, fs, type=type, interval=interval)
            case "real_imag":
                support.plot_Real_Imag(signal, freq, fs, type=type, interval=interval)
            case "bode":
                support.plot_bode(signal, freq, type=type, interval=interval)
            case _:
                print("Error: Invalid version")

    def make_signal_step(total_time , duration, sampling_rate , amplitude = 1,starting_time=0):
        """
        Generate a step signal with the given parameters.
    
        Parameters:
        - total_time (float): Total time duration of the signal.
        - duration (float): Duration of the step signal.
        - sampling_rate (int): Sampling rate of the signal.
        - amplitude (float, optional): Amplitude of the step signal (default: 1).
        - starting_time (float, optional): Starting time of the step signal (default: 0).
    
        Returns:
        - step (numpy.ndarray): Step signal.
        - t (numpy.ndarray): Time values corresponding to the step signal.
    
        Example:
        make_signal_step(total_time=5, duration=2, sampling_rate=1000, amplitude=1, starting_time=1)
        """
        step_signal_off_before = np.zeros(int(sampling_rate*(starting_time)), dtype=int)
        step_signal_on = np.ones(int(sampling_rate*duration), dtype=int)
        step_signal_off_after = np.zeros(int(sampling_rate*(total_time- starting_time - duration)), dtype=int)

        step = np.concatenate( (step_signal_off_before, step_signal_on , step_signal_off_after) ) * amplitude

        t = np.arange(0 , total_time , 1 / sampling_rate)

        if np.size(step) == np.size(t) - 1:
            step = np.concatenate([step , np.zeros(1)])


        return step, t

    def make_signal_ramp(total_time , duration, sampling_rate , amplitude = 1):
        """
        Generate a ramp signal with the given parameters.

        Parameters:
        - total_time (float): Total time duration of the signal.
        - duration (float): Duration of the ramp signal.
        - sampling_rate (int): Sampling rate of the signal.
        - amplitude (float, optional): Amplitude of the ramp signal (default: 1).

        Returns:
        - ramp (numpy.ndarray): Ramp signal.
        - t (numpy.ndarray): Time values corresponding to the ramp signal.

        Example:
        make_signal_ramp(total_time=5, duration=2, sampling_rate=1000, amplitude=1)
        """
        step_signal_on = np.ones(int(sampling_rate*duration), dtype=int)
        step_signal_off_after = np.zeros(int(sampling_rate*(total_time - duration)), dtype=int)

        step = np.concatenate( (step_signal_on , step_signal_off_after) )

        t = np.arange(0 , total_time , 1 / sampling_rate)

        ramp = step * t

        if np.size(ramp) == np.size(t) - 1:
            ramp = np.concatenate([ramp , np.zeros(1)])


        return ramp, t

    def convolve(sig1, sig2, fs):
        """
        Perform convolution of two signals.

        Parameters:
        - sig1 (numpy.ndarray): First signal.
        - sig2 (numpy.ndarray): Second signal.
        - fs (int): Sampling rate of the signals.

        Returns:
        - conv_sig (numpy.ndarray): Convolution of sig1 and sig2.
        - t (numpy.ndarray): Time values corresponding to the convolution.

        Example:
        convolve(signal1, signal2, fs=44100)
        """
        conv_sig = sig.convolve(sig1, sig2)
        t = (np.arange(0, len(conv_sig))) / fs

        return conv_sig, t

    def impulse_response(num, den):
        """
        Generate the impulse response of a moving average filter of order x.
        Parameters:
        - sampling_frequency (float): Sampling frequency in Hz.
        - time (float): Duration of the impulse response in seconds.
        - order (int): Order of the moving average filter.
        Returns:
        - impulse_response (numpy.ndarray): The impulse response of the moving average filter.
        Example:
        - impulse_response = moving_average_impulse_response(5, 44100, 0.1)
        """

        # Create the impulse response with x spikes
        impulse_response, time  = sig.impulse(system=(num, den))
        # Zero-pad the impulse response to match the desired duration
        
        return impulse_response, time

    def moving_average(data, window_size,fs):
        """
        input:
        - data: signal
        - window_size: size of the window
        output:
        smoothed data
        time
        """
        window = np.ones(window_size) / window_size
        smoothed_data = np.convolve(data, window, mode='same')
        t = (np.arange(0, len(data))) / fs
        return smoothed_data,t

class support:
    def plot_mag_phase(magnitude, phase, freq, fs=44100, type="stem", interval=None):
        """
        Plots the magnitude and phase spectra of a signal.
        Parameters:
        magnitude (array-like): The magnitude spectrum of the signal.
        phase (array-like): The phase spectrum of the signal.
        freq (array-like): The frequency range of the signal.
        fs (int, optional): The sampling frequency of the signal. Defaults to 44100.
        type (str, optional): The type of plot to generate. Can be "stem", "plot", or "both". Defaults to "stem".
        interval (tuple, optional): The frequency range to plot. Defaults to None.
        Returns:
        None
        """
        # Plot the magnitude spectrum
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 8))
        
        match type.lower():
            case "stem":
                # Plot the magnitude spectrum in the first subplot
                ax1.stem(freq, magnitude)
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Magnitude')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.stem(freq, phase)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Phase (radians)')
                ax2.grid(True)

            case "plot":
                # Plot the magnitude spectrum in the first subplot
                ax1.plot(freq, magnitude)
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Magnitude')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.plot(freq, phase)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Phase (radians)')
                ax2.grid(True)

            case "both":
                # Plot the magnitude spectrum in the first subplot
                ax1.stem(freq, magnitude)
                plt.Color = "red"
                ax1.plot(freq, magnitude)
                plt.Color = "blue"
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Magnitude')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.stem(freq, phase)
                ax2.plot(freq, phase)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Phase (radians)')
                ax2.grid(True)
            case _:
                print("Error: Invalid version")
        plt.subplots_adjust(hspace=0.5)
        if interval is not None:
            ax1.set_xlim(interval[0] , interval[1])
            ax2.set_xlim(interval[0] , interval[1])
        plt.show()
    
    def plot_bode(signal, freq, type="plot", interval=None):
        """
        Plots the Bode plot of a signal.
    
        Parameters:
        - signal (array-like): The complex signal to plot.
        - freq (array-like): The frequency range of the signal.
        - type (str, optional): The type of plot to generate. Can be "plot" or other types in the future. Defaults to "plot".
        - interval (tuple, optional): The frequency range to plot. Defaults to None.
    
        Returns:
        None
        """
        magnitude = 20 * np.log10(np.abs(signal))   
        phase = np.angle(signal)
        
        # Plot the magnitude spectrum
        first_peak_index = np.argmax(magnitude)
        print("First peak at: ", freq[first_peak_index], "Hz")
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 8))
        match type.lower():
            case "plot":
                # Plot the magnitude spectrum in the first subplot

                # plt.figure(figsize=(10, 6))
                # ax1.semilogx(freq, magnitude, label='Magnitude Spectrum (dB)', color='blue')
                # ax1.scatter(freq[first_peak_index], magnitude[first_peak_index], label='Magnitude Spectrum peaks', color='red', marker='o')
                # ax1.legend()
                ax1.plot(freq, magnitude)
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Magnitude (dB)')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.plot(freq, phase)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Phase (radians)')
                ax2.grid(True)
            case _:
                print("Error: Invalid version")
        plt.subplots_adjust(hspace=0.5)
        plt.title("Bode (Log Scale)")
        if interval is not None:
            ax1.set_xlim(interval[0] , interval[1])
            ax2.set_xlim(interval[0] , interval[1])
        plt.show()
    
    def plot_Real_Imag(signal, freq, fs=44100, type="stem", interval=None):
        """
        Plots the real and imaginary parts of a signal in the frequency domain.
        Args:
            signal (array-like): The signal to plot.
            freq (array-like): The frequency values corresponding to the signal.
            fs (int, optional): The sampling frequency of the signal. Defaults to 44100.
            type (str, optional): The type of plot to create. Can be "stem", "plot", or "both". Defaults to "stem".
            interval (tuple, optional): The frequency range to plot. Defaults to None.
        Returns:
            None
        """
        # Plot the magnitude spectrum
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 8))
        
        match type.lower():
            case "stem":
                # Plot the magnitude spectrum in the first subplot
                ax1.stem(freq, signal.real)
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Real Part')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.stem(freq, signal.imag)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Imaginary Part')
                ax2.grid(True)
            case "plot":
                # Plot the magnitude spectrum in the first subplot
                ax1.plot(freq, signal.real)
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Real Part')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.plot(freq, signal.imag)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Imaginary Part')
                ax2.grid(True)
            case "both":
                # Plot the magnitude spectrum in the first subplot
                ax1.stem(freq, signal.real)
                plt.Color = "red"
                ax1.plot(freq, signal.real)
                plt.Color = "blue"
                ax1.set_xlabel('Frequency (Hz)')
                ax1.set_ylabel('Real Part')
                ax1.grid(True)

                # Plot the phase spectrum in the second subplot
                ax2.stem(freq, signal.imag)
                ax2.plot(freq, signal.imag)
                ax2.set_xlabel('Frequency (Hz)')
                ax2.set_ylabel('Imaginary Part')
                ax2.grid(True)
            case _:
                print("Error: Invalid version")
        plt.subplots_adjust(hspace=0.5)
        if interval is not None:
            ax1.set_xlim(interval[0] , interval[1])
            ax2.set_xlim(interval[0] , interval[1])
        plt.show()

    